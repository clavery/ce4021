# Given a list of transactions dictionaries please calculate the average spend for each customer and then output
# all transactions which are over the particular users average spend



class Transaction:

    def __init__(self, transaction_id, customer_id, currency, amount):
        self.transaction_id = transaction_id
        self.customer_id = customer_id,
        self.currency = currency
        self.amount = amount
        self.local_amount = 0

class CustomerProfile:

    def __init__(self, customerId):
        self.customerId = customerId
        self.transactions = []
        self.local_currency_spend = 0

    def get_average_local_currency_spend(self):
        return self.local_currency_spend/len(self.transactions)


class TransactionService:

    exchange_rates = {
        "usd": {
            "eur": .86,
            "gbp": .77,
            "usd": 1,
        },
        "gbp": {
            "eur": 1.12,
            "gbp": 1,
            "usd": 1.3
        },
        "eur": {
            "eur": 1,
            "gbp": .89,
            "usd": 1.16
        }
    }

    def __init__(self, default_currency ="eur"):
        self.default_currency = default_currency


    def get_flagged_transactions(self, transaction_records):
        """
           Given a list of transactions and exchange rates with return a list of transaction_ids
           which are above the average currency spend for the given customer id

           Args:
               transaction_record (list(<transaction_records>)) a list of transaction records in format {"transaction_id": <Any>,"customer_id": <Any>, "currency" : <string>, "amount" : <int>}

           Returns:
               A list of flagged transaction ids
        """

        customers = {}
        flagged_transaction = set()

        for transaction in transaction_records:
            local_amount = transaction.amount * self.exchange_rates[transaction.currency][self.default_currency]
            transaction.local_amount = local_amount
            customer_id = transaction.customer_id
            current_customer_profile = customers.get(customer_id, CustomerProfile(customer_id))
            current_customer_profile.local_currency_spend += local_amount
            current_customer_profile.transactions.append(transaction)
            customers[customer_id] = current_customer_profile

        for customer in customers.values():
            for transaction in customer.transactions:
                if transaction.local_amount > customer.get_average_local_currency_spend():
                    flagged_transaction.add(transaction.transaction_id)

        return flagged_transaction



ts = TransactionService()
ts_usd = TransactionService("usd")


def test_get_flagged_transactions_returns_empty_for_empty_transaction_list():
    ts = TransactionService()
    transaction_records = []

    expected = 0
    actual = len(ts.get_flagged_transactions(transaction_records))

    assert(expected == actual)

def test_get_flagged_transaction_returns_empty_list_if_customer_only_has_single_transaction():
    ts = TransactionService()
    transaction_records = [Transaction(1,1,"eur", 100)]

    expected = 0
    actual = len(ts.get_flagged_transactions(transaction_records))

    assert(expected == actual)

def test_get_flagged_transaction_returns_correct_list_for_non_empty_transaction_list():
    ts = TransactionService()
    transaction_records = [Transaction(1,1,"eur",1), Transaction(2,1,"eur", 2)]

    expected = {transaction_records[1].transaction_id}
    actual = ts.get_flagged_transactions(transaction_records)

    assert(expected == actual)

def test_get_flagged_transaction_returns_correct_list_for_non_empty_transaction_list_other_currencies():
    ts = TransactionService()
    transaction_records = [Transaction(1,1,"usd",1), Transaction(2,1,"eur", 2)]

    expected = {transaction_records[1].transaction_id}
    actual = ts.get_flagged_transactions(transaction_records)

    assert(expected == actual)

def test_get_flagged_transaction_returns_correct_list_for_non_empty_transaction_list_with_multiple_customers():
    ts = TransactionService()
    transaction_records = [Transaction(1,1,"eur",1), Transaction(2,1,"eur", 2),Transaction(3,2,"eur",1), Transaction(4,2,"eur", 2)]

    expected = {transaction_records[1].transaction_id, transaction_records[3].transaction_id}
    actual = ts.get_flagged_transactions(transaction_records)

    assert(expected == actual)

test_get_flagged_transaction_returns_correct_list_for_non_empty_transaction_list()
test_get_flagged_transaction_returns_correct_list_for_non_empty_transaction_list_other_currencies()
test_get_flagged_transaction_returns_correct_list_for_non_empty_transaction_list_with_multiple_customers()
test_get_flagged_transaction_returns_empty_list_if_customer_only_has_single_transaction()
test_get_flagged_transactions_returns_empty_for_empty_transaction_list()