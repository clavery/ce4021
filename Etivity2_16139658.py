def calculate_derivative(positive_coefficients=[], negative_coefficients = []):
    """
        Given a list of positive and negative coefficients of `x` returns the derivative of the function i.e. f(x) -> f'(x)

           Args:
               positive_coefficients: list of digits representing the positive coefficients of x ascending(starting at x^0, x^1 x2 ... x^n)
               negative_coefficients: list of digits representing the negative coefficients of x ascending(starting at x^-1 x^-2 x-3 ... x^-n)

           Returns:
               A tuple of positive coefficients and negative coefficients
    """
    d_positive_powers = [coefficient * (i + 1) for i, coefficient in enumerate(positive_coefficients[1:])]#start at index 1 as the index 0 value is eliminated during differentiation
    d_negative_powers = [coefficient * (-i-1) for i, coefficient in enumerate(negative_coefficients)]

    return d_positive_powers, d_negative_powers

def format_coefficients(coefficients):
    """
        Given a list of positive and negative coefficients of `x` returns formatted string

           Args:
               positive_coefficients: list of digits representing the positive coefficients (starting at x^0, x^1 x2 ... x^n)
               negative_coefficients: list of digits representing the negative coefficients (starting at x^-1 x^-2 x-3 ... x^-n)

           Returns:
               A string representation of the positive and negative coefficients
    """
    positive_coefficients = coefficients[0]
    negative_coefficient = coefficients[1]
    result = ""
    if positive_coefficients:
        for i, coefficient in enumerate(reversed(positive_coefficients)):
            if coefficient != 0:
                power = len(positive_coefficients)-i-1
                result += "({}x^{}) + ".format(str(coefficient), str(power))
    result = result[:-3]

    if negative_coefficient:
        result += " + "
        for i, coefficient in enumerate(negative_coefficient):
            if coefficient != 0:
                power = i-1
                result += "({}x^{}) + ".format(str(coefficient), str(power))

    result = result.replace("+ (-", "- (")
    result = result.replace("x^0", "")
    result = result.replace(" 1x", " x")
    result = result.replace("x^1)", "x)")
    result = result.replace(".0x","x")

    return result


import unittest

class TestEtivity1(unittest.TestCase):

    def test_deriviatives_excercise_1(self):
        function = [0,0,3]

        expected_result = [0,6]
        expected_format_result = "(6x)"

        result = calculate_derivative(function)
        format_result = format_coefficients(result)
        self.assertEqual(result[0], expected_result)
        self.assertEquals(format_result, expected_format_result)

    def test_deriviatives_excercise_2(self):
        function = [16,16,1]

        expected_result = [16,2]
        expected_format_result = "(2x) + (16)"

        result = calculate_derivative(function)
        format_result = format_coefficients(result)
        self.assertEquals(result[0], expected_result)
        self.assertEquals(format_result, expected_format_result)

    def test_deriviatives_excercise_3(self):
        function = [0,0,0,1,0,0,0,0,.5]

        expected_result = [0,0,3,0,0,0,0, 4]
        expected_format_result = "(4x^7) + (3x^2)"

        result = calculate_derivative(function)
        format_result = format_coefficients(result)
        self.assertEquals(result[0], expected_result)
        self.assertEquals(format_result, expected_format_result)

    def test_deriviatives_excercise_4(self):
        #using coefficient 1 to represent x^3 we are calculating d'(a)
        function = [0,1]

        expected_result = [1]
        expected_format_result = "(1)"

        result = calculate_derivative(function)
        format_result = format_coefficients(result)
        self.assertEquals(result[0], expected_result)
        self.assertEquals(format_result, expected_format_result)

    def test_deriviatives_excercise_4(self):
        function = [0] * 502
        function[501] = 1
        function[7]=3
        function[6]=-0.5
        function[5]=1
        function[3]=2
        function[2]=3
        function[0]=-1

        expected_result = [0] * 501
        expected_result[500] = 501
        expected_result[6] = 21
        expected_result[5] = -3
        expected_result[4] = 5
        expected_result[2] = 6
        expected_result[1] = 6
        expected_format_result = "(501x^500) + (21x^6) - (3x^5) + (5x^4) + (6x^2) + (6x)"

        result = calculate_derivative(function)
        format_result = format_coefficients(result)
        self.assertEquals(result[0], expected_result)
        self.assertEquals(format_result, expected_format_result)


if __name__ == '__main__':
    unittest.main()